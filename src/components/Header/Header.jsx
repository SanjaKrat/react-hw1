import React from 'react';
import styled from 'styled-components';

import Button from '../../common/Button/Button';
import Logo from './components/Logo/Logo';

const HeaderStyled = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: end;
	align-items: center;
	height: 75px;
	border: 1px solid lightpink;
	padding: 0 25px;
	margin-bottom: 15px;
`;

const Header = () => (
	<div className='container'>
		<HeaderStyled>
			<Logo />
			<p>Oleksandr</p>
			<Button buttonText='Logout' styles={{ marginLeft: '25px' }} />
		</HeaderStyled>
	</div>
);

export default Header;
