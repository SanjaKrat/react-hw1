import React from 'react';
import logo from './logo.svg';

const Logo = () => (
	<img style={{ marginRight: 'auto' }} src={logo} alt='logo' />
);

export default Logo;
