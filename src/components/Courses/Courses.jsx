import React, { useEffect, useState } from 'react';

import styled from 'styled-components';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import CreateCourse from '../CreateCourse/CreateCourse';

import { mockedCoursesList, mockedAuthorsList } from '../../constants';

const CoursesStyled = styled.div`
	border: 1px solid lightblue;
	box-sizing: border-box;
	padding: 15px;
	min-height: calc(100vh - 120px);
`;
const SearchWrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	width: 100%;
`;

const Courses = () => {
	const [searchQuery, setSearchQuery] = useState('');
	const [courses, setCourses] = useState(mockedCoursesList);
	const [coursesToDisplay, setCoursesToDisplay] = useState(courses);
	const [allAuthors, setAllAuthors] = useState(mockedAuthorsList);
	const [openAddCourses, setOpenAddCourses] = useState(false);

	const courseAuthors = (courseAuthorsIds, allAuthors) => {
		const authors = [];
		courseAuthorsIds.forEach((id) => {
			allAuthors.forEach((a) => {
				if (a.id === id) {
					authors.push(a.name);
				}
			});
		});
		return authors;
	};

	useEffect(() => {
		if (!searchQuery) setCoursesToDisplay(courses);
	}, [courses, searchQuery]);

	const searchCourses = () => {
		const filtered = [];
		coursesToDisplay.forEach((course) => {
			if (
				course.title.toLowerCase().includes(searchQuery) ||
				course.id.toLowerCase().includes(searchQuery)
			) {
				filtered.push(course);
			}
		});
		setCoursesToDisplay(filtered);
	};

	return openAddCourses ? (
		<CreateCourse
			allAuthors={allAuthors}
			setAllAuthors={setAllAuthors}
			courses={courses}
			setCourses={setCourses}
			setOpenAddCourses={setOpenAddCourses}
		/>
	) : (
		<CoursesStyled className='container'>
			<SearchWrapper>
				<SearchBar
					setSearchQuery={setSearchQuery}
					searchCourses={searchCourses}
				/>
				<div>
					<Button
						buttonText='Add new course'
						onClick={() => setOpenAddCourses(true)}
					/>
				</div>
			</SearchWrapper>
			{coursesToDisplay.length ? (
				coursesToDisplay.map((course) => (
					<CourseCard
						key={course.id}
						title={course.title}
						description={course.description}
						creationDate={course.creationDate}
						duration={course.duration}
						authors={courseAuthors(course.authors, allAuthors)}
					/>
				))
			) : (
				<p>No courses with title or id '{searchQuery}'</p>
			)}
		</CoursesStyled>
	);
};

export default Courses;
