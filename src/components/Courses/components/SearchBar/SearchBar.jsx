import React from 'react';

import styled from 'styled-components';

import Button from '../../../../common/Button/Button';
import Input from '../../../../common/Input/Input';

const SearchWrapper = styled.div`
	display: flex;
	align-items: flex-start;
	margin-bottom: 25px;
`;

const SearchBar = ({ setSearchQuery, searchCourses }) => (
	<SearchWrapper>
		<Input placeholderText='Enter course name...' onChange={setSearchQuery} />
		<div>
			<Button
				buttonText='Search'
				styles={{ marginLeft: '20px' }}
				onClick={() => searchCourses()}
			/>
		</div>
	</SearchWrapper>
);

export default SearchBar;
