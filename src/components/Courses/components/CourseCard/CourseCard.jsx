import React from 'react';
import styled from 'styled-components';

import Button from '../../../../common/Button/Button';

import pipeDuration from '../../../../helpers/pipeDuration';
import dateGenerator from '../../../../helpers/dateGenerator';

const Course = styled.div`
	border: 1px solid green;
	border-radius: 5px;
	box-sizing: border-box;
	padding: 15px;
	margin-bottom: 15px;
	display: flex;
	align-items: stretch;
`;
const Description = styled.div`
	width: min-content;
	flex-grow: 1;
	display: flex;
	flex-direction: column;
`;
const Title = styled.h2`
	margin-top: 0;
`;
const DescriptionText = styled.p`
	margin-top: 0;
	margin-bottom: 0;
`;
const BoldTitle = styled.span`
	font-weight: bold;
`;
const CardInfo = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	margin-left: 30px;
	height: 100%;
	min-width: 330px;
	max-width: 45%;
`;
const CardInfoItem = styled.p`
	margin: 0;
	line-height: 2rem;
`;
const CardInfoItemWrapper = styled.div`
	margin-bottom: 20px;
`;

const CourseCard = ({
	title,
	description,
	creationDate,
	duration,
	authors,
}) => (
	<Course>
		<Description>
			<Title>{title}</Title>
			<DescriptionText>{description}</DescriptionText>
		</Description>
		<div>
			<CardInfo>
				<CardInfoItemWrapper>
					<CardInfoItem>
						<BoldTitle>Authors: </BoldTitle>
						{authors.length ? authors.join(', ') : 'Anonym'}
					</CardInfoItem>
					<CardInfoItem>
						<BoldTitle>Duration: </BoldTitle> {pipeDuration(duration)} hours
					</CardInfoItem>
					<CardInfoItem>
						<BoldTitle>Created: </BoldTitle>
						{dateGenerator(creationDate).replaceAll('/', '.')}
					</CardInfoItem>
				</CardInfoItemWrapper>
				<Button
					buttonText='Show course'
					styles={{ alignSelf: 'center', marginTop: 'auto' }}
				/>
			</CardInfo>
		</div>
	</Course>
);

export default CourseCard;
