import React, { useState } from 'react';
import styled from 'styled-components';
import { v4 as uuidv4 } from 'uuid';

import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import Textarea from '../../common/Textarea/Textarea';

import pipeDuration from '../../helpers/pipeDuration';
import dateGenerator from '../../helpers/dateGenerator';

const CreateCourseStyled = styled.div`
	border: 1px solid lightblue;
	box-sizing: border-box;
	padding: 15px;
	min-height: max-content;
	min-height: calc(100vh - 110px);
`;
const TitleDescriptionWrapper = styled.div`
	display: flex;
	flex-direction: column;
`;
const TitleWrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: flex-end;
	margin-bottom: 15px;
`;
const H3 = styled.h3`
	text-align: center;
`;
const MainWrapper = styled.div`
	display: flex;
	flex-direction: row;
	gap: 70px;
	margin-top: 15px;
	border: 1px solid black;
	padding: 15px;
`;
const MainLeft = styled.div`
	display: flex;
	flex-direction: column;
	width: 45%;
`;
const MainRight = styled.div`
	display: flex;
	flex-direction: column;
	min-width: 370px;
`;
const AuthorItem = styled.div`
	display: flex;
	align-items: center;
	margin-bottom: 10px;
`;
const AuthorName = styled.div`
	width: 200px;
`;
const NewAuthorButtonWrapper = styled.div`
	text-align: center;
	margin-top: 15px;
`;
const DurationWrapper = styled.div`
	margin-top: 40px;
`;
const DurationText = styled.p`
	font-size: 1.5rem;
`;
const Span = styled.span`
	font-size: 2rem;
	font-weight: bold;
`;

const CreateCourse = ({
	allAuthors,
	setAllAuthors,
	courses,
	setCourses,
	setOpenAddCourses,
}) => {
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [newAuthor, setNewAuthor] = useState('');
	const [availableAuthors, setAvailableAuthors] = useState(allAuthors);
	const [duration, setDuration] = useState('');

	const createAuthor = (authorName) => {
		const newAuthor = {
			id: uuidv4(),
			name: authorName,
		};
		setAvailableAuthors([...availableAuthors, ...[newAuthor]]);
		setAllAuthors([...allAuthors, ...[newAuthor]]);
		setNewAuthor('');
	};

	const addAuthor = (author) => {
		setCourseAuthors([
			...courseAuthors,
			...allAuthors.filter((a) => author.id === a.id),
		]);
		setAvailableAuthors(availableAuthors.filter((a) => a.id !== author.id));
	};

	const deleteAuthor = (author) => {
		setAvailableAuthors([
			...availableAuthors,
			...courseAuthors.filter((a) => author.id === a.id),
		]);
		setCourseAuthors(courseAuthors.filter((a) => author.id !== a.id));
	};

	const createCourse = () => {
		if (title && description && duration && courseAuthors.length) {
			const course = {
				id: uuidv4(),
				title,
				description,
				duration: Number(duration),
				creationDate: dateGenerator(new Date()),
				authors: courseAuthors.map((a) => a.id),
			};
			setCourses([...courses, ...[course]]);
			setOpenAddCourses(false);
		} else {
			alert('Please, fill in all fields');
		}
	};

	return (
		<CreateCourseStyled className='container'>
			<TitleDescriptionWrapper>
				<TitleWrapper>
					<Input
						labelText='Title'
						onChange={setTitle}
						placeholderText='Enter title'
						id='title'
						value={title}
					/>
					<div>
						<Button buttonText='Create course' onClick={() => createCourse()} />
					</div>
				</TitleWrapper>
				<Textarea
					labelText='Description'
					placeholderText='Enter description'
					id='description'
					onChange={setDescription}
					value={description}
				/>
			</TitleDescriptionWrapper>

			<MainWrapper>
				<MainLeft>
					<div>
						<H3>Add author</H3>
						<Input
							labelText='Author name'
							placeholderText='Enter author name...'
							id='name'
							value={newAuthor}
							onChange={setNewAuthor}
						/>
						<NewAuthorButtonWrapper>
							<Button
								buttonText='Create author'
								onClick={() => createAuthor(newAuthor)}
							/>
						</NewAuthorButtonWrapper>
					</div>
					<DurationWrapper>
						<H3>Duration</H3>
						<Input
							labelText='Duration'
							placeholderText='Enter duration in minutes...'
							id='duration'
							type='number'
							value={duration}
							onChange={setDuration}
						/>
						<DurationText>
							Duration: <Span>{pipeDuration(duration)}</Span> hours
						</DurationText>
					</DurationWrapper>
				</MainLeft>
				<MainRight>
					<div>
						<H3>Authors</H3>
						{availableAuthors.length ? (
							availableAuthors.map((author) => (
								<AuthorItem key={author.id}>
									<AuthorName>{author.name}</AuthorName>
									<div>
										<Button
											buttonText='Add author'
											styles={{ marginLeft: '25px' }}
											onClick={() => addAuthor(author)}
										/>
									</div>
								</AuthorItem>
							))
						) : (
							<p>Author list is empty</p>
						)}
					</div>
					<div>
						<H3>Course authors</H3>
						{courseAuthors.length ? (
							courseAuthors.map((author) => (
								<AuthorItem key={author.id}>
									<AuthorName>{author.name}</AuthorName>
									<div>
										<Button
											buttonText='Delete author'
											styles={{ marginLeft: '25px' }}
											onClick={() => deleteAuthor(author)}
										/>
									</div>
								</AuthorItem>
							))
						) : (
							<p>Author list is empty</p>
						)}
					</div>
				</MainRight>
			</MainWrapper>
		</CreateCourseStyled>
	);
};

export default CreateCourse;
