import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
`;
const InputStyled = styled.input`
	height: 27px;
	border: 1px solid orange;
	border-radius: 5px;
	outline: none;
	font-family: inherit;
	font-size: inherit;
	padding-left: 10px;
`;
const Label = styled.label`
	margin-bottom: 10px;
`;

const Input = ({
	labelText,
	placeholderText,
	id,
	type = 'text',
	onChange,
	value,
}) => (
	<Wrapper>
		{labelText ? <Label htmlFor={id}>{labelText}</Label> : <></>}
		<InputStyled
			type={type}
			id={id}
			min='0'
			placeholder={placeholderText}
			onChange={(evt) => onChange(evt.target.value)}
			value={value}
		/>
	</Wrapper>
);

export default Input;
