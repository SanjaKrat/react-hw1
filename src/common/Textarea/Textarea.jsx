import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
`;

const TextareaStyled = styled.textarea`
	border: 1px solid yellow;
	border-radius: 5px;
	outline: none;
	font-family: inherit;
	font-size: inherit;
	padding: 10px;
`;
const Label = styled.label`
	margin-bottom: 5px;
`;

const Textarea = ({ labelText, placeholderText, onChange, value = '' }) => (
	<Wrapper>
		<Label htmlFor='textarea'>{labelText}</Label>
		<TextareaStyled
			id='textarea'
			placeholder={placeholderText}
			onChange={(evt) => onChange(evt.target.value)}
			rows='5'
			value={value}
		/>
	</Wrapper>
);

export default Textarea;
