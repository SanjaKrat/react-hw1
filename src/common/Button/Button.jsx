import React from 'react';
import styled from 'styled-components';

const ButtonStyled = styled.button`
	cursor: pointer;
	font-size: 16px;
	border: 1px solid purple;
	border-radius: 5px;
	padding: 5px 25px;
`;

const Button = ({ buttonText, onClick, styles = {} }) => (
	<ButtonStyled onClick={onClick} style={styles}>
		{buttonText}
	</ButtonStyled>
);

export default Button;
