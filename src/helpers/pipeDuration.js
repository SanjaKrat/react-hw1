const pipeDuration = (duration) => {
	const hours = duration < 60 ? 0 : Math.floor(duration / 60);
	const minutes = duration - hours * 60;
	return `${hours.toString().length > 1 ? hours.toString() : `0${hours}`}:${
		minutes.toString().length > 1 ? minutes.toString() : `0${minutes}`
	}`;
};

export default pipeDuration;
