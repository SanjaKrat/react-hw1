const dateGenerator = (dateToGenerate) => {
	const date = new Date(dateToGenerate);
	return `${addZeroIfNeeded(date.getDate())}/${addZeroIfNeeded(
		date.getMonth() + 1
	)}/${date.getFullYear()}`;
};

function addZeroIfNeeded(number) {
	if (number.toString().length < 2) {
		return `0${number}`;
	}
	return number;
}

export default dateGenerator;
